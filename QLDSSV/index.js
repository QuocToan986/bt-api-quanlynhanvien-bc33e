var dssv = [];

var BASE_URL = "https://63063e00c0d0f2b801191139.mockapi.io";

/*===================HÀM GỌI API===================*/
function renderDSSVService() {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      dssv = res.data;
      renderTable(dssv);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

/*===================GỌI API===================*/
renderDSSVService();

/*===================CHỨC NĂNG THÊM===================*/
function themSV() {
  var dataForm = layThongTinTuForm();
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: dataForm,
  })
    .then(function (res) {
      renderDSSVService();
      tatLoading();
      console.log(res);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

/*===================CHỨC NĂNG XÓA===================*/
function xoaSV(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      renderDSSVService();
      tatLoading();
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

/*===================CHỨC NĂNG SỬA===================*/
function suaSV(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      showThongTinLenForm(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

/*===================CHỨC NĂNG CẬP NHẬT===================*/
function capNhatSV() {
  var dataForm = layThongTinTuForm();
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${dataForm.ma}`,
    method: "PUT",
    data: dataForm,
  })
    .then(function (res) {
      renderDSSVService();
      tatLoading();
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

/*===================CHỨC NĂNG RESET===================*/
function resetForm() {
  document.getElementById("formQLSV").reset();
}

/*===================CHỨC NĂNG SEARCH===================*/
function btnSearch() {
  var searchSVInput = document.getElementById("txtSearch").value;
  var searchSV = dssv.filter(function (item) {
    return item.ten.toUpperCase().includes(searchSVInput.toUpperCase());
  });
  renderTable(searchSV);
}

/*===================CHỨC NĂNG RESET TABLE===================*/
function btnReSetTable() {
  renderTable(dssv);
}
