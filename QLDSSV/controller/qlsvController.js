function layThongTinTuForm() {
  var ma = document.getElementById("txtMaSV").value;
  var ten = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var hinhAnh = document.getElementById("txtImg").value;

  var sv = new SinhVien(ma, ten, email, hinhAnh);
  return sv;
}

function renderTable(arr) {
  var contentHTML = "";
  arr.forEach((item) => {
    var content = `
          <tr>
          <td>${item.ma}</td>
          <td>${item.ten}</td>
          <td>${item.email}</td>
          <td> <img src= ${item.hinhAnh} style = "width: 80px" xalt=""/> </td>
          <td>
          <button class="btn btn-danger" onclick = "xoaSV(${item.ma})">Xóa</button>
          <button class="btn btn-primary" onclick = "suaSV(${item.ma})">Sửa</button>
          </td>
          </tr>
          `;
    contentHTML += content;
  });
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function batLoading() {
  document.getElementById("loading").style.display = "flex";
}

function tatLoading() {
  document.getElementById("loading").style.display = "none";
}

function showThongTinLenForm(data) {
  document.getElementById("txtMaSV").value = data.ma;
  document.getElementById("txtTenSV").value = data.ten;
  document.getElementById("txtEmail").value = data.email;
  document.getElementById("txtImg").value = data.hinhAnh;
}
